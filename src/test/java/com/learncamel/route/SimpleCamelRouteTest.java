package com.learncamel.route;

import org.apache.camel.Exchange;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.test.spring.CamelSpringBootRunner;
import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

@ActiveProfiles("DEV")
@RunWith(CamelSpringBootRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest
public class SimpleCamelRouteTest {

    @Autowired
    ProducerTemplate producerTemplate;
    @Autowired
    Environment environment;

    @BeforeClass
    public static void startCleanUp() throws IOException {

        FileUtils.cleanDirectory(new File("data/DEV/input"));
        FileUtils.deleteDirectory(new File("data/output"));
    }

    @Test
    public void testMoveFile() throws InterruptedException {

        String message = "type,sku#,itemdescription,price\n" +
                "ADD,100,Sumsung TV,500\n" +
                "ADD,101,LG TV,500";
        String filename = "fileTest.txt";
        producerTemplate.sendBodyAndHeader(environment.getProperty("fromRoute")
                                        ,message, Exchange.FILE_NAME,filename);

        Thread.sleep(3000);
        File outFile = new File("data/output/"+filename);
        assertTrue(outFile.exists());

    }
}
