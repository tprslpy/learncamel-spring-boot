package com.learncamel.service;

import com.learncamel.domain.Item;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.Exchange;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ItemService {

    public String readItemObj(Exchange exchange) {
        Item itemObj = exchange.getIn().getBody(Item.class);
        log.info("item obj toString = " + itemObj.toString());

        return itemObj.toString();
    }
}
