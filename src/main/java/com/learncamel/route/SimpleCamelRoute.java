package com.learncamel.route;

import com.learncamel.domain.Item;
import com.learncamel.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class SimpleCamelRoute extends RouteBuilder {
    @Autowired
    Environment environment ;
    @Override
    public void configure() throws Exception {

        log.info("Start Camel Route");
        //from("{{startRoute}}")
        //       .log("Timer Invoked and the body ${body}")
        //        .pollEnrich("{{fromRoute}}")
        //        .to("{{toRoute}}");

        /*from("{{startRoute}}")
                .log("Timer Invoked and the body " + environment.getProperty("message"))
                .pollEnrich("{{fromRoute}}")
                .to("{{toRoute}}");
        */


        BindyCsvDataFormat bindy = new BindyCsvDataFormat(Item.class);
        from("{{startRoute}}").streamCaching()
                .log("Timer Invoked and the body " + environment.getProperty("message"))
                .choice()
                    .when(header("env").isNotEqualTo("MOCK"))
                        .pollEnrich("{{fromRoute}}")
                    .otherwise()
                        .log("MOCK env flow and the body is ${body}")
                .end()
                .to("{{toRoute}}")
                .unmarshal(bindy)
//                .unmarshal().csv()
                .bean(ItemService.class, "readItemObj")
                .log("The unmarshal object is ${body} ")
                .split(body())
                    .log("Record is ${body}")
                .end();
        log.info("Ending Camel Route");

    }
}
